package info.fandroid.pavlik

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.graphics.Color
import android.graphics.SurfaceTexture
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Surface
import android.view.View
import android.view.ViewTreeObserver
import android.webkit.MimeTypeMap
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.PlaybackParameters
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.RawResourceDataSource
import com.google.android.exoplayer2.util.Util
import info.fandroid.pavlik.fragment.OnPavlikListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

class MainActivity : AppCompatActivity(), OnPavlikListener {



    var ifAudio = false


    lateinit var player: SimpleExoPlayer


    lateinit var dataSourceFactory: DataSource.Factory


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //playerView.videoSurfaceView.visibility = View.INVISIBLE

        logSentFriendRequestEvent()


        val navController: NavController = findNavController(R.id.my_nav_host_fragment)


        bottomNavigation.setupWithNavController(navController)


        val resId = resources.getIdentifier("exo_shutter", "id", packageName)
        findViewById<View>(resId).setBackgroundColor(Color.TRANSPARENT)


        val bandwidthMeter = DefaultBandwidthMeter()
        val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)
        val trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)

        // 2. Create the player
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector)

        playerView.player = player

        // Produces DataSource instances through which media data is loaded.
        dataSourceFactory = DefaultDataSourceFactory(this,
                Util.getUserAgent(this, "Pavlik"), bandwidthMeter)

        // This is the MediaSource representing the media to be played.
        playerView.useController = false

        player.playWhenReady = true




        player.addListener(object : Player.DefaultEventListener() {


            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                super.onPlayerStateChanged(playWhenReady, playbackState)
                Log.d("playbackState", "state: $playbackState")
                when (playbackState) {
                    1 -> {
                        //imageView.visibility = View.VISIBLE
                        hidePlayer()
                    }
                    2 -> {
                        //imageView.visibility = View.VISIBLE
                        hidePlayer()
                    }
                    3 -> {
                        if (!ifAudio) {
                            showPlayer()
                            //imageView.visibility = View.INVISIBLE
                        } else {
                            hidePlayer()
                            //imageView.visibility = View.VISIBLE
                        }
                    }
                }
            }
        })
    }


    override fun onVideoPlay(resId: Int) {
        player.stop()

        ifAudio = false

        val uri = RawResourceDataSource.buildRawResourceUri(resId)

        val videoSource = ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri)

        player.prepare(videoSource)
    }

    override fun onVideoPlayerStop() {
        player.stop()

    }


    override fun onAudioPlay(resId: Int) {
        onVideoPlay(resId)
        ifAudio = true
    }

    override fun onAudioPlayerStop() {

    }

    override fun onDestroy() {
        super.onDestroy()
        player.release()
    }


    fun hidePlayer() {
//
//        animate(playerView, 1f, 0f)
        animate(imageView, 1f, 300).start()



    }

    fun showPlayer() {

        AnimatorSet().apply {
            play(animate(imageView, 1f, 0f, 300))

            start()
        }
        //playerFrame.visibility = View.VISIBLE
    }


    fun animate(view: View, from: Float, to: Float, duration: Long): ObjectAnimator {
        val animator = ObjectAnimator.ofFloat(view, "alpha", from, to)
        animator.duration = duration
        return animator
    }

    fun animate(view: View, to: Float, duration: Long): ObjectAnimator {
        val animator = ObjectAnimator.ofFloat(view, "alpha", to)
        animator.duration = duration
        return animator
    }


    fun animate(view: View, from: Float, to: Float): ObjectAnimator {
        return animate(view, from, to, 1000)
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    fun logSentFriendRequestEvent() {

        var logger: AppEventsLogger = AppEventsLogger.newLogger(applicationContext)
        logger.logEvent("sentFriendRequest")
    }


}
