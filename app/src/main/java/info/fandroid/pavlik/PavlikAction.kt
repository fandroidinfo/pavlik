package info.fandroid.pavlik

import android.os.Bundle
import androidx.core.os.bundleOf


object PavlikAction {

    const val ACTION = "action"

    const val WATCH = "watch"
    const val SAY = "say"

    const val ALL = "all"
    const val EAT = "eat"
    const val DRINK = "drink"
    const val CARTOON = "cartoon"
    const val PLAY = "play"
    const val SLEEP = "sleep"
    const val WALK = "walk"
    const val PEE = "pee"
    const val POOP = "poop"

    fun action(action: String): Bundle {
        return bundleOf(ACTION to action)
    }


}
