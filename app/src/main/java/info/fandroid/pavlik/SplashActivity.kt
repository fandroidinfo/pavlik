package info.fandroid.pavlik

import android.content.Intent
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.ProgressBar


class SplashActivity : AppCompatActivity() {

    private var imageView: ImageView? = null
    private var animationRight: Animation? = null
    private var animationLeft: Animation? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        imageView = findViewById<View>(R.id.ivSplashIcon) as ImageView
        animationRight = AnimationUtils.loadAnimation(baseContext, R.anim.rotate_right)
        animationLeft = AnimationUtils.loadAnimation(baseContext, R.anim.rotate_left)
    }

    private fun initFunctionality() {

        imageView!!.startAnimation(animationLeft)
        animationLeft!!.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {

            }

            override fun onAnimationEnd(animation: Animation) {

                imageView!!.startAnimation(animationRight)

            }

            override fun onAnimationRepeat(animation: Animation) {

            }
        })
        animationRight!!.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {

            }

            override fun onAnimationEnd(animation: Animation) {
                imageView!!.visibility = View.INVISIBLE
                val intent = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(intent)
                finish()

            }

            override fun onAnimationRepeat(animation: Animation) {

            }
        })
    }

    override fun onResume() {
        super.onResume()
        initFunctionality()
    }
}
