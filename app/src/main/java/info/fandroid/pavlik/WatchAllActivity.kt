package info.fandroid.pavlik

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.RawResourceDataSource
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.activity_watch_all.*

class WatchAllActivity : AppCompatActivity() {

    lateinit var player: SimpleExoPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_watch_all)



        val resId = resources.getIdentifier("exo_shutter", "id", packageName)
        findViewById<View>(resId).setBackgroundColor(Color.WHITE)

        val bandwidthMeter = DefaultBandwidthMeter()
        val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)
        val trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)

        // 2. Create the player
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector)

        playerView.player = player




        // Produces DataSource instances through which media data is loaded.
        val dataSourceFactory = DefaultDataSourceFactory(this,
                Util.getUserAgent(this, "Pavlik"), bandwidthMeter)

        // This is the MediaSource representing the media to be played.
        val uri = RawResourceDataSource.buildRawResourceUri(R.raw.video_look_all)
        Log.d("uri", uri.toString())
        val videoSource = ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri)

        // Prepare the player with the source.
        player.prepare(videoSource)

        player.playWhenReady = true

        player.addListener(object : Player.DefaultEventListener() {


            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                super.onPlayerStateChanged(playWhenReady, playbackState)
                Log.d("playbackState", "state: $playbackState")
                when (playbackState) {
                    1 -> {
                        imageView.visibility = View.VISIBLE
                    }
                    2 -> {
                        imageView.visibility = View.VISIBLE
                    }
                    3 -> {
                            imageView.visibility = View.INVISIBLE

                    }
                }
            }
        })
    }



    override fun onStop() {
        super.onStop()
        player.stop()
    }
}
