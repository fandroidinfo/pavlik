package info.fandroid.pavlik.fragment


import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController

import info.fandroid.pavlik.R
import kotlinx.android.synthetic.main.fragment_drink.*

/**
 * A simple [Fragment] subclass.
 *
 */

class DrinkFragment : PlayerFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_drink, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val action = arguments!!.getString("action")


        btnWantEatGreen.setOnClickListener {
            findNavController().popBackStack()
        }

        btnWantEatYellow.setOnClickListener {
            if (action == "watch") {
                onPavlikListener.onVideoPlay(R.raw.video_drink)
            } else {
                onPavlikListener.onAudioPlay(R.raw.audio_drink)
            }
        }


        ivTea.setOnClickListener {
            if (action == "watch") {
                onPavlikListener.onVideoPlay(R.raw.video_drink_tea)
            } else {
                onPavlikListener.onAudioPlay(R.raw.audio_tea)
            }

            animate(it)
        }

        ivMilk.setOnClickListener {
            if (action == "watch") {
                onPavlikListener.onVideoPlay(R.raw.video_drink_milk)
            } else {
                onPavlikListener.onAudioPlay(R.raw.audio_milk)
            }

            animate(it)
        }

        ivWater.setOnClickListener {
            if (action == "watch") {
                onPavlikListener.onVideoPlay(R.raw.video_drink_water)
            } else {
                onPavlikListener.onAudioPlay(R.raw.audio_water)
            }

            animate(it)
        }

        ivJuice.setOnClickListener {
            if (action == "watch") {
                onPavlikListener.onVideoPlay(R.raw.video_drink_juice)
            } else {
                onPavlikListener.onAudioPlay(R.raw.audio_juice)
            }

            animate(it)
        }
    }

    fun animate(view: View) {
        val scaleDown = ObjectAnimator.ofPropertyValuesHolder(view,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.2f))
        scaleDown.duration = 500
        scaleDown.repeatMode = ValueAnimator.REVERSE
        scaleDown.repeatCount = 3
        scaleDown.start()
    }
}
