package info.fandroid.pavlik.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import info.fandroid.pavlik.R
import kotlinx.android.synthetic.main.fragment_eat.*
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.util.Log
import android.view.animation.Animation
import androidx.navigation.fragment.findNavController


/**
 * A simple [Fragment] subclass.
 *
 */
class EatFragment : PlayerFragment() {



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_eat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val action = arguments!!.getString("action")




        btnWantEatGreen.setOnClickListener {
            findNavController().popBackStack()
        }

        btnWantEatYellow.setOnClickListener {
            if (action == "watch") {
                onPavlikListener.onVideoPlay(R.raw.video_eat)
            } else {
                onPavlikListener.onAudioPlay(R.raw.audio_eat)
            }
        }


        ivPorridge.setOnClickListener {

            if (action == "watch") {
                onPavlikListener.onVideoPlay(R.raw.video_eat_porridge)
            } else {
                onPavlikListener.onAudioPlay(R.raw.audio_porridge)
            }

            animate(it)



        }

        ivCake.setOnClickListener {

            if (action == "watch") {
                onPavlikListener.onVideoPlay(R.raw.video_eat_cake)
            } else {
                onPavlikListener.onAudioPlay(R.raw.audio_cake)
            }
            animate(it)

        }

        ivFruits.setOnClickListener {

            if (action == "watch") {
                onPavlikListener.onVideoPlay(R.raw.video_eat_fruits)
            } else {
                onPavlikListener.onAudioPlay(R.raw.audio_fruits)
            }
            animate(it)

        }

        ivCookies.setOnClickListener {
            if (action == "watch") {
                onPavlikListener.onVideoPlay(R.raw.video_eat_cookies)
            } else {
                onPavlikListener.onAudioPlay(R.raw.audio_cookies)
            }
            animate(it)

        }


    }

    fun animate(view: View) {
        val scaleDown = ObjectAnimator.ofPropertyValuesHolder(view,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.2f))
        scaleDown.duration = 500
        scaleDown.repeatMode = ValueAnimator.REVERSE
        scaleDown.repeatCount = 3
        scaleDown.start()
    }

}
