package info.fandroid.pavlik.fragment


import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.get
import androidx.navigation.findNavController
import info.fandroid.pavlik.PavlikAction

import info.fandroid.pavlik.R
import kotlinx.android.synthetic.main.fragment_home.*
import android.os.Build
import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import java.util.*
import android.content.Intent.getIntent
import android.content.Intent
import android.content.SharedPreferences


/**
 * A simple [Fragment] subclass.
 *
 */
class HomeFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {



        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)


    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)






        btnWatch.setOnClickListener {
            view.findNavController().navigate(R.id.watchWantFragment)

        }

        btnSay.setOnClickListener {
            view.findNavController().navigate(R.id.sayWantFragment)
        }



    }





    override fun onResume() {
        super.onResume()
        activity!!.findViewById<BottomNavigationView>(R.id.bottomNavigation).menu[0].isChecked = true

    }
}
