package info.fandroid.pavlik.fragment

interface OnPavlikListener {

    fun onVideoPlay(resId: Int)

    fun onVideoPlayerStop()

    fun onAudioPlay(resId: Int)

    fun onAudioPlayerStop()
}