package info.fandroid.pavlik.fragment


import android.content.Context
import android.support.v4.app.Fragment


/**
 * A simple [Fragment] subclass.
 *
 */
abstract class PlayerFragment : Fragment() {

    protected lateinit var onPavlikListener: OnPavlikListener

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        onPavlikListener = activity as OnPavlikListener
    }

    override fun onStop() {
        super.onStop()
        onPavlikListener.onVideoPlayerStop()
    }
}
