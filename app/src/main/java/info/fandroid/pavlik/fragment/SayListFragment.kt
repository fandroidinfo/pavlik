package info.fandroid.pavlik.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import info.fandroid.pavlik.PavlikAction

import info.fandroid.pavlik.R
import kotlinx.android.synthetic.main.fragment_say_list.*


/**
 * A simple [Fragment] subclass.
 *
 */
class SayListFragment : PlayerFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_say_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnEat.setOnClickListener {
            view.findNavController().navigate(R.id.eatFragment, PavlikAction.action(PavlikAction.SAY))
        }

        btnDrink.setOnClickListener {
            view.findNavController().navigate(R.id.drinkFragment, PavlikAction.action(PavlikAction.SAY))
        }

        btnCartoon.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_cartoon)
        }

        btnPlay.setOnClickListener {
            Log.d("want to", "play")
            onPavlikListener.onAudioPlay(R.raw.audio_play)
        }

        btnSleep.setOnClickListener {
            Log.d("want to", "sleep")
            onPavlikListener.onAudioPlay(R.raw.audio_sleep)
        }

        btnWalk.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_walk)
        }


        btnPee.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_pee)
        }


        btnPoop.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_poop)
        }
    }


}
