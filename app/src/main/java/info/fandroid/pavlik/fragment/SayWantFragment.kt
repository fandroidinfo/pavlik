package info.fandroid.pavlik.fragment


import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import info.fandroid.pavlik.R
import kotlinx.android.synthetic.main.fragment_say_want.*


/**
 * A simple [Fragment] subclass.
 *
 */
class SayWantFragment : PlayerFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_say_want, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        btnCartoon.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_cartoon)
            animate(imgCartoon)
        }

        btnPlay.setOnClickListener {
            Log.d("want to", "play")
            onPavlikListener.onAudioPlay(R.raw.audio_play)
            animate(imgPlay)
        }

        btnSleep.setOnClickListener {
            Log.d("want to", "sleep")
            onPavlikListener.onAudioPlay(R.raw.audio_sleep)
            animate(imgSleep)
        }

        btnWalk.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_walk)
            animate(imgWalk)
        }


        btnPee.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_pee)
            animate(imgPee)
        }


        btnPoop.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_poop)
            animate(imgPoop)
        }




        imgCartoon.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_cartoon)
            animate(it)
        }

        imgPlay.setOnClickListener {
            Log.d("want to", "play")
            onPavlikListener.onAudioPlay(R.raw.audio_play)
            animate(it)
        }

        imgSleep.setOnClickListener {
            Log.d("want to", "sleep")
            onPavlikListener.onAudioPlay(R.raw.audio_sleep)
            animate(it)
        }

        imgWalk.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_walk)
            animate(it)
        }


        imgPee.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_pee)
            animate(it)
        }


        imgPoop.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_poop)
            animate(it)
        }



        ivPorridge2.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_porridge)
            animate(it)
        }

        ivCake2.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_cake)
            animate(it)
        }

        ivFruits2.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_fruits)
            animate(it)
        }

        ivCookies2.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_cookies)
            animate(it)
        }



        ivTea2.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_tea)
            animate(it)
        }

        ivMilk2.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_milk)
            animate(it)
        }

        ivWater2.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_water)
            animate(it)
        }

        ivJuice2.setOnClickListener {
            onPavlikListener.onAudioPlay(R.raw.audio_juice)
            animate(it)
        }
    }

    fun animate(view: View) {
        val scaleDown = ObjectAnimator.ofPropertyValuesHolder(view,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.2f))
        scaleDown.duration = 500
        scaleDown.repeatMode = ValueAnimator.REVERSE
        scaleDown.repeatCount = 3
        scaleDown.start()
    }


}
