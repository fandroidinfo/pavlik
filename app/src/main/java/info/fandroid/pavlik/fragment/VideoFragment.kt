package info.fandroid.pavlik.fragment


import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.annotation.IntegerRes
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import info.fandroid.pavlik.PavlikAction

import info.fandroid.pavlik.R
import kotlinx.android.synthetic.main.fragment_video.*
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.analytics.AnalyticsListener
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.TrackSelection
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.util.Util.getUserAgent
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.RawResourceDataSource
import com.google.android.exoplayer2.util.Util


/**
 * A simple [Fragment] subclass.
 *
 */
class VideoFragment : Fragment() {


    lateinit var player: SimpleExoPlayer

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_video, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val action: String = arguments!!.getString(PavlikAction.ACTION)

        video.text = " video \n$action"



        val bandwidthMeter = DefaultBandwidthMeter()
        val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)
        val trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)

        // 2. Create the player
        player = ExoPlayerFactory.newSimpleInstance(context, trackSelector)

        playerView.player = player

        playerView.controllerAutoShow = false

        playerView.hideController()


        // Produces DataSource instances through which media data is loaded.
        val dataSourceFactory = DefaultDataSourceFactory(context,
                Util.getUserAgent(context, "Pavlik"), bandwidthMeter)

        // This is the MediaSource representing the media to be played.
        val uri = RawResourceDataSource.buildRawResourceUri(R.raw.video_look_all)
        Log.d("uri", uri.toString())
        val videoSource = ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri)

        // Prepare the player with the source.
        player.prepare(videoSource)

        player.playWhenReady = true

    }

    override fun onStop() {
        super.onStop()
        player.stop()
    }

}
