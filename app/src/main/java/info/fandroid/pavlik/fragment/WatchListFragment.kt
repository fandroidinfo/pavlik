package info.fandroid.pavlik.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import info.fandroid.pavlik.PavlikAction

import info.fandroid.pavlik.R
import info.fandroid.pavlik.R.id.*
import kotlinx.android.synthetic.main.fragment_watch_list.*

/**
 * A simple [Fragment] subclass.
 *
 */

class WatchListFragment : PlayerFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_watch_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        btnEat.setOnClickListener {
            view.findNavController().navigate(R.id.eatFragment, PavlikAction.action(PavlikAction.WATCH))
        }

        btnDrink.setOnClickListener {
            view.findNavController().navigate(R.id.drinkFragment, PavlikAction.action(PavlikAction.WATCH))
        }


        btnCartoon.setOnClickListener {
            onPavlikListener.onVideoPlay(R.raw.video_cartoon)
        }

        btnPlay.setOnClickListener {
            onPavlikListener.onVideoPlay(R.raw.video_play)
        }

        btnSleep.setOnClickListener {
            onPavlikListener.onVideoPlay(R.raw.video_sleep)
        }

        btnWalk.setOnClickListener {
            onPavlikListener.onVideoPlay(R.raw.video_walk)
        }


        btnPee.setOnClickListener {
            onPavlikListener.onVideoPlay(R.raw.video_pee)
        }


        btnPoop.setOnClickListener {
            onPavlikListener.onVideoPlay(R.raw.video_poop)
        }
    }




}
