package info.fandroid.pavlik.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import info.fandroid.pavlik.PavlikAction

import info.fandroid.pavlik.R
import kotlinx.android.synthetic.main.fragment_watch_want.*



/**
 * A simple [Fragment] subclass.
 *
 */
class WatchWantFragment : PlayerFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_watch_want, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        onPavlikListener.onVideoPlay(R.raw.video_want)

        btnWantTo.setOnClickListener {
            onPavlikListener.onVideoPlayerStop()
            view.findNavController().navigate(R.id.watchListFragment)

        }

        btnWatchAll.setOnClickListener {
            view.findNavController().navigate(R.id.watchAllActivity)

        }


    }


}
